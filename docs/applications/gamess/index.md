# GAMESS

The General Atomic and Molecular Electronic Structure System (GAMESS)
is a general ab initio quantum chemistry package. Please see 
the [GAMESS home page](https://www.msg.chem.iastate.edu/gamess/) and
[official documentation](https://www.msg.chem.iastate.edu/gamess/documentation.html)
for more information.

## Using GAMESS at NERSC

GAMESS is provided on Cori via the `gamess` module. Users should invoke GAMESS
using the `rungms` script which is added to the user's `$PATH` after loading a
`gamess` module. The syntax for the `rungms` script is:

```console
rungms <input file> 00 <number of processes>
```

The directory `$GAMESS_DIR/hsw/tests/standard` includes a large number of
example inputs files that are included with the GAMESS installation.
